###############################################################################
# Author: @CaseySteele24  
# Purpose: Collect NBA Stats from 2012-2015
# Date: July 2015
###############################################################################

#-----------------------------------------------------------------------
# set up script level basics
#-----------------------------------------------------------------------

## libraries
library(XML)

## directory for the project
DIR <- "C:/Users/Casey/Documents/R"
setwd(DIR)



#-----------------------------------------------------------------------
# Create a function that will take a year and return a dataframe
#-----------------------------------------------------------------------

GrabBallers <- function(S) {
  
  # The function takes parameter S which is a string and represents the Season
  # Returns: data frame    
  
  ## create the URL
  URL <- paste("http://www.basketball-reference.com/boxscores/201504150BRK.html", sep="")
  
  ## grab the page -- the table is parsed nicely
  tables <- readHTMLTable(URL)
  ds.ballers <- tables$stats
  
  ## determine if the HTML table was well formed (column names are the first record)
  ## can either read in directly or need to force column names
  ## and 
  
  ## I don't like dealing with factors if I don't have to
  ## and I prefer lower case
  for(i in 1:ncol(ds.ballers)) {
    ds.ballers[,i] <- as.character(ds.ballers[,i])
    names(ds.ballers) <- tolower(colnames(ds.ballers))
  }
  
  ## fix a couple of the column names
  #colnames(ds.ballers)
  #names(ds.ballers)[10] <- "plusmin"
  #names(ds.ballers)[17] <- "spct"
  
  ## finally fix the columns - NAs forced by coercion warnings
  for(i in c(1, 3, 6:18)) {
    ds.ballers[,i] <- as.numeric(ds.ballers[, i])
  }
  
  ## convert toi to seconds, and seconds/game
  #ds.ballers$seconds <- (ds.ballers$toi*60)/ds.ballers$gp
  
  ## remove the header and totals row
  #ds.skaters <- ds.skaters[!is.na(ds.skaters$rk), ]
  #ds.skaters <- ds.skaters[ds.skaters$tm != "TOT", ]
  
  ## add the year
  ds.ballers$season <- S
  
  ## return the dataframe
  return(ds.ballers)
  
}




#-----------------------------------------------------------------------
# Use the function to loop over the seasons and piece together
#-----------------------------------------------------------------------

## define the seasons -- 2005 dataset doesnt exist
## if I was a good coder I would trap the error, but this works
SEASON <- as.character(2012:2015)


## create an empy dataset that we will append to
dataset <- data.frame()

## loop over the seasons, use the function to grab the data
## and build the dataset
for (S in SEASON) {
  
  temp <- GrabBallers(S)
  dataset <- rbind(dataset, temp)
  print(paste("Completed Season ", S, sep=""))
  
  ## pause the script so we don't kill their servers
  Sys.sleep(3)
  
}

## save the dataset
write.table(dataset, "BBR Scrap.csv", sep=",",
            row.names=F)
}